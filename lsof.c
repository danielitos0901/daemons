#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h> 
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h> 
#include <sys/stat.h>

int is_number(char* input){
  int length = strlen (input);
  for (int i=0;i<length; i++)//iteramos sobre el string
    if (!isdigit(input[i]))//si el char en turno no es digito
      return -1;//regresamos -1 ya que no es un numero
  return 1;//termino el for asi que todos fueron numeros
}

char* get_path(char* sympath){
  struct stat sb;
  ssize_t r = INT_MAX;
  int linkSize = 0;
  const int growthRate = 255;

  char* linkTarget = NULL;

  // get length of the pathname the link points to
  if (lstat(sympath, &sb) == -1) {   // could not lstat: insufficient permissions on directory?
      return "";
  }

  // read the link target into a string
  linkSize = sb.st_size + 1 - growthRate;
  while (r >= linkSize) { // i.e. symlink increased in size since lstat() or non-POSIX compliant filesystem
      // allocate sufficient memory to hold the link
      linkSize += growthRate;
      free(linkTarget);
      linkTarget = malloc(linkSize);
      if (linkTarget == NULL) {           // insufficient memory
          fprintf(stderr, "insufficient memory\n");
          return "";
      }

      // read the link target into variable linkTarget
      r = readlink(sympath, linkTarget, linkSize);
      if (r < 0) {        // readlink failed: link was deleted?
          return "";
      }
  }
  linkTarget[r] = '\0';   // readlink does not null-terminate the string
  printf("link: %s\n", linkTarget);
  return linkTarget;
}

int main(void) { 
  struct dirent *de;  // Pointer for directory entry 
  
  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir("/proc/"); 
  if (dr == NULL)  // opendir returns NULL if couldn't open directory 
  { 
    printf("Could not open current directory\n" ); 
    return 0; 
  } 
  
  // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html 
  // for readdir() 
  while ((de = readdir(dr)) != NULL){
    //getting only the proccess files
    if(is_number(de->d_name) != 1)
      continue;
    //skipping the current thread;
    if(getpid() == atoi(de->d_name)){
      continue;
    }

    struct dirent *pid_fd;//Pointer for directory with the files
    //string with the path
    char path[strlen(de->d_name) + 10];
    //cleaning the string
    memset(path, 0, sizeof(path));
    
    //making the path
    strcat(path, "/proc/");
    strcat(path, de->d_name);
    strcat(path, "/fd/\0");
    
    printf("File path:%s\n", path);

    char exe[strlen(de->d_name) + 10];
    memset(exe, 0, sizeof(exe));
    strcat(exe, "/proc/");
    strcat(exe, de->d_name);
    strcat(exe, "/exe\0");
    printf("Exe path:\n");
    get_path(exe);
    printf("Files:\n");
    // opendir() returns a pointer of DIR type.  
    DIR *dr_fd = opendir(path); 
    
    if (dr_fd == NULL)  // opendir returns NULL if couldn't open directory 
    { 
      printf("Could not open current directory to files\n" ); 
      continue; //skip that file 
    } 
    
    while ((pid_fd = readdir(dr_fd)) != NULL){
      //Skip '.' and '..' files in all carpets (and any hidden file)
      if(pid_fd->d_name[0] == '.')
        continue;
      //string with the path
      char path_fd[strlen(path) + strlen(pid_fd->d_name) + 1];
      //cleaning the string
      memset(path_fd, 0, sizeof(path_fd));
      //making the fd path
      strcat(path_fd, path);
      strcat(path_fd, pid_fd->d_name);
      strcat(path_fd, "\0");
      //printf("File sympath: %s\n", path_fd);
      get_path(path_fd);
    }
    memset(path, 0, sizeof(path));
    memset(exe, 0, sizeof(exe));
    closedir(dr_fd);     
    printf("------------------------------------------\n");
  }
  
  closedir(dr);     
  return 0; 
} 
