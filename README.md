# Daemons

## ps
Denomio ps que muestra toda la informacion concerniente a todos los procesos activos en sistema.

## lsof
Demonio lsof que muestra todos los archivos abiertos por todos los procesos del sistema.

## Instrucciones

Se anexan dos archivos por cada demonio, uno con terminacion <daemon>d.c que contiene el demonio y un archivo <daemon>.c que
hace la ejecutacion del programa en terminal para su ver su funcionamiento.
cualquiera de los archivos c se compilan de la forma usual.
```bash
$gcc <daemon>.c -o 
```
```bash
$./<daemon>
```
 de la forma mas simplificada
```bash
$gcc <daemon>.c
```
```bash
$./a.out
```

